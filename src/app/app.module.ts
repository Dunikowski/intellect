import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginBarComponent } from './login-bar/login-bar.component';
import { MainPageService } from './main.page.services';
import { HeaderBarComponent } from './header-bar/header-bar.component';
import { SearchComponent } from './search/search.component';
import { SelectListComponent } from './select-list/select-list.component';
import { RadioButtonComponent } from './radio-button/radio-button.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { LatestNewsCarouselComponent } from './latest-news-carousel/latest-news-carousel.component';
import { ServiceTvComponent } from './service-tv/service-tv.component';
import { FooterComponent } from './footer/footer.component';
import { OrderNewsletterComponent } from './order-newsletter/order-newsletter.component';
import { MajoritycomponentComponent } from './share/component/majoritycomponent/majoritycomponent.component';

const routes: Routes = [
  {path: '', component: AppComponent},
  {path: '**', component: AppComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    LoginBarComponent,
    HeaderBarComponent,
    SearchComponent,
    SelectListComponent,
    RadioButtonComponent,
    LatestNewsCarouselComponent,
    ServiceTvComponent,
    FooterComponent,
    OrderNewsletterComponent,
    MajoritycomponentComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CarouselModule,
    RouterModule.forRoot(routes)
  ],
  providers: [MainPageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
