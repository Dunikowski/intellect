import { Component, OnInit } from '@angular/core';
import { IFooter } from './IFooter';
import { MainPageService } from '../main.page.services';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  _data: IFooter;
  constructor(private mainpageservice: MainPageService) {}

  ngOnInit() {
    this.mainpageservice.getFooter().subscribe(
      (data: IFooter): void => {
        this._data = data;
      },
      error =>
        console.error('Error retrive data for Footer section: ', <any>error)
    );
  }
}
