export interface IList {
  list_name: string;
  list_items: string[];
}

export interface IInfo {
  copyryght: string;
  info_details: string[];
  performer: string[];
}

export interface IFooter {
  background_img: string;
  list: IList[];
  info: IInfo;
}
