import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { IOrderNewsletter } from './IOrderNewsletter';
import { MainPageService } from '../main.page.services';

@Component({
  selector: 'app-order-newsletter',
  templateUrl: './order-newsletter.component.html',
  styleUrls: ['./order-newsletter.component.scss']
})
export class OrderNewsletterComponent implements OnInit {

  constructor(
    private mainpageservice: MainPageService,
    private _fb: FormBuilder
  ) {}

  newsletterForm: FormGroup;
  _data: IOrderNewsletter;
  _errors: any;
  permision = [
    new FormControl(false, [Validators.requiredTrue]),
    new FormControl(false, [Validators.requiredTrue])
  ];

  ngOnInit() {
    this.mainpageservice.getOrderNewsletter().subscribe(
      (data: IOrderNewsletter): void => {
        this._data = data;
        this.isSetForm();
        this._errors = this.newsletterForm.get('email');
      }
    );
  }

  isSetForm(): void {
    this.newsletterForm = this._fb.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      permision_0: this.permision[0],
      permision_1: this.permision[1]
    });
  }

  send(): void {
    console.log('send', this.newsletterForm);
    this.newsletterForm.reset();
  }
}
