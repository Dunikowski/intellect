export interface IInfo {
  background_img: string;
  secure_img: string;
  headimg: string;
  text: string;
  forwarding: string;
}

export interface IPermission {
  chackbox_img: string;
  chackbox_text: string[];
}
export interface IForm {
  headimg: string;
  placeholder: string;
  arrow_button: string;
  unsubscribe: string;
  permission: IPermission;
}

export interface IOrderNewsletter {
  info: IInfo;
  form: IForm;
}
