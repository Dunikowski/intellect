import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderNewsletterComponent } from './order-newsletter.component';

describe('OrderNewsletterComponent', () => {
  let component: OrderNewsletterComponent;
  let fixture: ComponentFixture<OrderNewsletterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderNewsletterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderNewsletterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
