
import { IData } from "./IData";

export interface IHeaderBarData {

    data: IData[],
    logo_img: string,
    basket_img: string,
    amount: number
}