import { Component, OnInit } from '@angular/core';
import { MainPageService } from '../main.page.services';
import { IHeaderBarData } from './IHeaderBarData';



@Component({
  selector: 'app-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.scss']
})
export class HeaderBarComponent implements OnInit {
  _data: IHeaderBarData = <IHeaderBarData>{};
  errorMessage: string;
  constructor(private mainpageservice: MainPageService) { }

  ngOnInit() {
    this.mainpageservice
      .getHeaderBarData()
      .subscribe((data: IHeaderBarData) => this._data = data,
        error => this.errorMessage = <any>error
      );
  }
}
