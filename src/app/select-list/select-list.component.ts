import { Component, OnInit, ElementRef, forwardRef, HostListener, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import { ISelect} from '../search/ISearchSection';


@Component({
  selector: 'app-select-list',
  templateUrl: './select-list.component.html',
  styleUrls: ['./select-list.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SelectListComponent), multi: true }
  ]
})

export class SelectListComponent implements OnInit, ControlValueAccessor {
  @Input() selectData: ISelect;

  value: any;

  isListVisible = false;
  isSelected: number;
  propagateChange: any = () => {};

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {}

  constructor(private elementref: ElementRef) {}

  @HostListener('document:click', ['$event']) clickout(event) {
    if (!this.elementref.nativeElement.contains(event.target)) {
      this.isListVisible = false;
    }
  }

  ngOnInit() {}

   selectItem(item: any): void {
    this.value = item;
    this.propagateChange(this.value);
    this.isListVisible = false;
   }

   getSelectedOption(option) {
     if (this.value === option) {
      return true;
     } else {
       return false;
     }
  }

  toggleList(): void {
    this.isListVisible = !this.isListVisible;
  }
}
