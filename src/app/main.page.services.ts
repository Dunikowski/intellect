import { Injectable } from '@angular/core';
import { ILoginBarData } from './login-bar/ILoginBarData';
import { IHeaderBarData } from './header-bar/IHeaderBarData';
import { ISearchSection, ISearchCarousel } from './search/ISearchSection';
import { INewsCarousel } from './latest-news-carousel/ILatestNewsCarousel';
import { IServiceTV } from './service-tv/IServiceTV';
import { IOrderNewsletter } from './order-newsletter/IOrderNewsletter';
import { IFooter } from './footer/IFooter';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';


@Injectable()
export class MainPageService {

  constructor(private _http: HttpClient) {}

  getLoginBarData(): Observable<ILoginBarData> {
    return this._http.get<ILoginBarData>('api/login_bar.json');
  }

  getHeaderBarData(): Observable<IHeaderBarData> {
    return this._http.get<IHeaderBarData>('api/header_bar.json');
  }

  getSearchSection(): Observable<ISearchSection> {
    return this._http.get<ISearchSection>('api/search_section.json');
  }

  getSearchCarousel(): Observable<ISearchCarousel> {
    return this._http.get<ISearchCarousel>('api/search_carousel.json');
  }

   getLatestNewsCarousel(): Observable<INewsCarousel> {
     return this._http.get<INewsCarousel>('api/latest_news_carousel.json')
     .catch(this.handleError);
   }

  getServiceTV(): Observable<IServiceTV> {
    return this._http.get<IServiceTV>('api/service_tv.json')
    .catch(this.handleError);
  }

  getOrderNewsletter(): Observable<IOrderNewsletter> {
    return this._http.get<IOrderNewsletter>('api/order_newsletter.json')
    .catch(this.handleError);
  }

  getFooter(): Observable<IFooter> {
    return this._http.get<IFooter>('api/footer.json')
    .catch(this.handleError);
  }
  private handleError (err: HttpErrorResponse) {
    return Observable.throw(err.message);
  }
}
