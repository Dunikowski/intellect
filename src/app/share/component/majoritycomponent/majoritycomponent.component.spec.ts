import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MajoritycomponentComponent } from './majoritycomponent.component';

describe('MajoritycomponentComponent', () => {
  let component: MajoritycomponentComponent;
  let fixture: ComponentFixture<MajoritycomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MajoritycomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MajoritycomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
