import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import { IRadioLabel } from '../search/ISearchSection';
@Component({
  selector: 'app-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => RadioButtonComponent), multi: true }
  ]
})
export class RadioButtonComponent implements OnInit, ControlValueAccessor {
@Input() data_radio: IRadioLabel[];
@Input() myForm: any;

itemSelected: string;
propagateChange: any = () => {};

writeValue(value: any) {
  if (value !== this.itemSelected) {
    this.itemSelected = value;
  }
}

  constructor() { }

  registerOnChange() {}

  click(i: any): void {
    this.myForm.controls['search_radio'].setValue(i);
    this.itemSelected = i;
  }
  registerOnTouched() {}
  ngOnInit() {}
}
