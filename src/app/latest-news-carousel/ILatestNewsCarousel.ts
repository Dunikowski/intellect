
export interface IContentItem {
  url_img: string;
  title: string;
  name: string;
  type_text: string;
}

export interface INCarouselItems {
  item: string;
  content_1: IContentItem;
  content_2: IContentItem;
}

export interface INewsCarousel {
  main_text: string;
  second_text: string;
  button_prev: string;
  button_next: string;
  carousel_items: INCarouselItems[];
}
