import { Component, OnInit } from '@angular/core';
import { MainPageService } from '../main.page.services';
import { INewsCarousel } from './IlatestNewsCarousel';

@Component({
  selector: 'app-latest-news-carousel',
  templateUrl: './latest-news-carousel.component.html',
  styleUrls: ['./latest-news-carousel.component.scss']
})
export class LatestNewsCarouselComponent implements OnInit {
  constructor(private mainpageservice: MainPageService) {}

  _data: INewsCarousel;

  customOptions: any = {
    loop: true,
    items: 3,
    margin: 43,
    dotsEach: false,
    autoplay: true,
    smartSpeed: 1000
  };
  ngOnInit() {
    this.mainpageservice.getLatestNewsCarousel().subscribe(
      (data: INewsCarousel): void => {
        this._data = data;
      },
      error =>
        console.error('Error retrive data for search carousel: ', <any>error)
    );
  }
}
