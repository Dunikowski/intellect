export interface ILoginBarData {
  facebook_text: string;
  YouTube_text: string;
  login_text_1: string;
  login_text_2: string;
  login_text_3: string;
  image_src_facebook: string;
  image_src_youtube: string;
  image_src_login: string;
  href_facebook: string;
  href_youtube: string;
  href_login: string;
}
