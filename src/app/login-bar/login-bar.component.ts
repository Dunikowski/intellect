import { Component, OnInit } from '@angular/core';
import { ILoginBarData } from './ILoginBarData';
import { MainPageService } from '../main.page.services';


@Component({
  selector: 'app-login-bar',
  templateUrl: './login-bar.component.html',
  styleUrls: ['./login-bar.component.scss']
})

export class LoginBarComponent implements OnInit {
  login_bar: ILoginBarData;
  errorMessage: string;

  constructor(private mainpageservice: MainPageService) {
  }
  ngOnInit() {
    this.mainpageservice
      .getLoginBarData()
      .subscribe((i: ILoginBarData) => this.login_bar = i,
        error => this.errorMessage = <any>error);
  }
}

