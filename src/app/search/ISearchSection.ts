
export interface ISelect {
    icon_button_select: string;
    select_options: string[];
}
export interface IRadioLabel {
    item: string;
}
export interface ISearchSection {
    main_text: string;
    background_image: string;
    input_text_area: string;
    icon_search: string;
    select: ISelect;
    radio_label: IRadioLabel[];
}
export interface ICarouselItems {
    url_img: string;
    date: string;
    text: string;
    id: string;
}
export interface ISearchCarousel {
    main_text: string;
    button_text: string;
    carousel_items: ICarouselItems[];
}
