import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { MainPageService } from '../main.page.services';
import { ISearchSection, ISelect, IRadioLabel, ISearchCarousel } from './ISearchSection';
import { SlidesOutputData } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {

  _data: ISearchSection = <ISearchSection>{};
  _data_SearchCarousel: ISearchCarousel;
  _radio: IRadioLabel[];
  _selectData: ISelect;
  isClassVisible = false;
  public myForm: FormGroup;
  payLoad = '';
  customOptions: any = {
   loop: true,
   items: 3,
   margin: 43,
   dotsEach: true,
   autoplay: true,
   smartSpeed: 1000
  };
  activeSlides = '0';

  constructor(private mainpageservice: MainPageService, private _fb: FormBuilder) {

  }

  isAction(i: any): void {
    if (i.type === 'focus') {
      this.isClassVisible = true;
    } else {

      if (i.target.value.length > 0) {
        this.isClassVisible = true;

      } else {
        this.isClassVisible = false;
      }
    }
  }

  isSetForm() {
    this.myForm = this._fb.group({
      search: '',
      _select: new FormControl(this._selectData.select_options[0]),
      search_radio: new FormControl(this._radio[0])
    });
  }

  ngOnInit() {
    this.mainpageservice.getSearchSection()
      .subscribe((data) => {
         this._data = data;
         this._selectData = data.select;
         this._radio = this._data.radio_label;
         this.isSetForm();
        },
        error => console.error('Error retrive data for search section: ', <any>error));
        this.mainpageservice.getSearchCarousel()
        .subscribe((data) => {
          this._data_SearchCarousel = data;
         },
         error => console.error('Error retrive data for search carousel: ', <any>error));
  }

  send(): void {
    this.payLoad = JSON.stringify(this.myForm.value);
    console.log('Send data search-form: ', this.payLoad);
    this.isClassVisible = false;
    this.isSetForm();
  }
      getPassedData(data: SlidesOutputData) {
        this.activeSlides = data.startPosition.toString();
      }
}
