import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceTvComponent } from './service-tv.component';

describe('ServiceTvComponent', () => {
  let component: ServiceTvComponent;
  let fixture: ComponentFixture<ServiceTvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceTvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceTvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
