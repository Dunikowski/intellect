import { Component, OnInit } from '@angular/core';
import { MainPageService } from '../main.page.services';
import { IServiceTV } from './IServiceTV';

@Component({
  selector: 'app-service-tv',
  templateUrl: './service-tv.component.html',
  styleUrls: ['./service-tv.component.scss']
})
export class ServiceTvComponent implements OnInit {
_data: IServiceTV;
  constructor(private mainpageservice: MainPageService) {}

  ngOnInit() {
    this.mainpageservice.getServiceTV()
    .subscribe((data: IServiceTV): void => {
     this._data = data;
    },
    error => console.error('Error retrive data for ServiceTV section: ', error));
  }

}
