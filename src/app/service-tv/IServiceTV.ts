export interface ITextTV {
  head_1: string;
  head_2: string;
  text: string;
}

export interface INavBar {
  nav_bar_item: string;
}

export interface IServiceTV {
  background_image: string;
  text_tv: ITextTV;
  nav_bar_items: INavBar[];
}
